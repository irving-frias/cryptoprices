<?php

namespace Drupal\cryptoprices\Commands;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drush\Commands\DrushCommands;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use Drupal\media\Entity\Media;
use Drupal\Core\File\FileSystemInterface;

/**
 * Provides commands to control cryptoprices actions.
 */
class CryptoPricesCommands extends DrushCommands {
  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  protected function getEntityTypeManager() {
    return $this->entityTypeManager;
  }

  /**
   * CryptoPricesCommands constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entityTypeManager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Drush command to import content from api.
   *
   * @command cryptoprices:import
   * @aliases cryptoprices-import
   * @usage cryptoprices:import
   */
  public function import() {
    $node_storage = $this->entityTypeManager->getStorage('node');
    $media_storage = $this->entityTypeManager->getStorage('media');

    echo $this->convert(memory_get_usage(true));
    echo "\n";

    $url = 'https://api.coingecko.com/api/v3/coins/';
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HTTPGET, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response_json = curl_exec($ch);
    curl_close($ch);
    echo "\n";
    $response = json_decode($response_json, true);

    if (!isset($response['error'])) {
      foreach ($response as $key => $value) {
        $nid = $node_storage->getQuery()
            ->accessCheck(FALSE)
            ->condition('type', ['crypto_coin'], 'IN')
            ->condition('field_crypto_id', $value["id"], 'IN')
            ->execute();
        $mid = $media_storage->getQuery()
            ->accessCheck(FALSE)
            ->condition('bundle', ['crypto_image'], 'IN')
            ->condition('name', $value['name'], 'IN')
            ->execute();
      
        if (empty($mid)) {
          $image_data = file_get_contents($value['image']['small']);
          $file_repository = \Drupal::service('file.repository');
          $processed_name = strtolower(str_replace(' ', '-', $value['name']));
          $image = $file_repository->writeData($image_data, "public://" . $processed_name . ".png", FileSystemInterface::EXISTS_REPLACE);

          $image_media = Media::create([
            'name' => $value['name'],
            'bundle' => 'crypto_image',
            'field_media_image' => [
              'target_id' => $image->id(),
              'alt' => t($value['name']),
              'title' => t($value['name']),
            ],
            'uid' => 1,
          ]);

          $image_media->save();
          dump("New Media image: " . $processed_name . ".jpeg");
        } else {
          foreach ($mid as $mid_key => $mid_value) {
            $image_media = Media::load($mid_key);
          }
        }

        if (empty($nid)) {
          $node = Node::create([
            'type' => 'crypto_coin',
            'field_crypto_id' => $value["id"],
            'title' => $value["name"],
            'field_crypto_name' => $value["name"],
            'field_crypto_price' => $value['market_data']['current_price']['usd'],
            'field_crypto_image' => $image_media,
            'field_crypto_symbol' => $value["symbol"],
            'uid' => 1,
          ]);

          $node->save();
          $message = t('Created Node @id.', [
            '@id' => $node->id(),
          ]);
          \Drupal::messenger()->addMessage($message);
        } else {
          foreach ($nid as $nid_index => $nid_value) {
            $modify_node = Node::Load($nid_value);
            $modify_node->field_crypto_price = $value['market_data']['current_price']['usd'];
            $modify_node->uid = 1;

            $modify_node->save();
            $message = t('Updated Node @id.', [
              '@id' => $modify_node->id(),
            ]);
            \Drupal::messenger()->addMessage($message);
          }
        }
      }
    }
  }

  public function convert($size)
  {
    $unit=array('b','kb','mb','gb','tb','pb');
    return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
  }
}